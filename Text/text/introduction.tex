\chapter{Introduction}\label{sec:Introduction}
\section{Motivation}
\par In the last few years machine learning methods using deep neural networks have conquered many fields such as computer vision~\cite{CNNSurvey}, speech recognition~\cite{lstm} and artificial intelligence for board games~\cite{alphazero} among others~\cite{NAS_RL}.
\par These networks can be constructed in many different ways.
To find networks that perform well after training on specific datasets, many decisions regarding hyperparameters need to be made by the persons responsible.
This circumstance entails that this task requires a lot of expert knowledge, experience and trials.
Qualified labor is sought after and trials need computational resources which is both linked to high costs.
\par To this end, many ways of automating this process have come up, such as genetic algorithms and reinforcement learning (see \nameref{chap:relatedWork}).
\par This work aims to evaluate the suitability of a new reinforcement learning method called Upside Down Reinforcement Learning (UDRL) for a gradient-based learning approach to finding optimal neural architectures.
Until now the approaches that use reinforcement to solve this problem employ a technique called Q-learning~\cite{NAS_RL}\cite{NASdesignRL}\cite{blockNAS}.
Here the memory requirement increases exponentially with the number of dimensions of the action space~\cite{dqn}.
\par Upside Down Reinforcement Learning on the other hand only needs constant space and time by predicting continuous actions.

\par The question this work aims to answer is:
\begin{framed}
    \noindent\textit{Is the performance of UDRL better than a genetic algorithm when applied on neural architecture search for supervised learning tasks?}
\end{framed}

\section{Contributions}
The extent of this work encompasses the following steps:
\begin{itemize}
    \item Reduce the search space to a selection of different layers and limit the amount of layers and parameters
    \item Create an abstraction that maps numerical parameters to neural networks
    \item Implement an Upside Down Reinforcement Learning (UDRL) algorithm for neural architecture search with different approaches
    \item Implement a genetic algorithm with the same goal
    \item Evaluate and compare the approaches of UDRL to each other and the genetic algorithm
\end{itemize}

\section{Roadmap}
The structure of the rest of the thesis can be organized into the following items:
\begin{itemize}
    \item Chapter~\ref{chap:background} introduces and explains the prerequisites.
    \item Chapter~\ref{chap:relatedWork} documents related work, i.e.\ other approaches that attempt to solve the automated search for neural architectures.
    \item Chapter~\ref{chap:methods} describes the implementation and design decisions that render the application of the algorithms possible.
    \item Chapter~\ref{chap:experiments} discusses the results of different approaches and compares them to the genetic algorithm.
    \item Chapter~\ref{chap:conclusion} concludes this thesis, summarizes the results and presents possible avenues for future work.
\end{itemize}

\chapter{Background}\label{chap:background}
\section{Neural Networks}
\subsection{The Perceptron}
Regarding neural networks everything started with the perceptron which took inspiration from the brain~\cite{perceptron-origin}.
It is an algorithm that learns a binary classifier using a linear function.
This means that if there is a linear separable problem it can learn the parameters describing a line that divides the set into two classes.

\begin{figure}[H]
   \centering
   \includegraphics[width=0.5\textwidth]{figures/perceptron}
   \caption{A linear separator (green) can divide the dataset perfectly}
   \label{fig:perceptron}
\end{figure}

To accomplish this feat a perceptron only needs one parameter for the bias \textit{b} and a weight-vector \textit{w} with the same number of dimension as the space that has to be separated.
\par The formula assigning the class is the following:
\[
f(x) =
  \begin{cases}
    1 & \quad \text{if } w \cdot x + b > 0\\
    0 & \quad \text{else}
  \end{cases}
\]
In two dimensions it is transformed to:
\[
f((x_1, x_2)) =
  \begin{cases}
    1  & \quad \text{if } w_1 x_1 + w_2 x_2 + b > 0\\
    0 & \quad \text{else}
  \end{cases}
\]
\par The goal is to learn the parameters of the perceptron, so that it begins to classify all points correctly or, if the classes are not separable, minimizes the number of wrongly classified data points.
To reach this goal there must be a defined error function, e.g. the number of wrongly classified points.
But in the following, we will consider linearly separable problems.
This is done by updating each parameter by going through every point and changing the weights depending on which class the point belongs to.
\[
    w_{i+1} = w_i + \eta d x_i
\]
where
\[
    d =
    \begin{cases}
        +1 & \quad \text{if class is 1}\\
        -1 & \quad \text{if class is 0}
    \end{cases}
\]
and $\eta$ is the learning rate \cite{perceptron}.
This method uses a gradient descent to find a local optimum.
If the steps of descent are too big, i.e. the learning rate too high, it can overshoot easily.
On the other hand if it is too low, learning takes more steps.

\subsection{Neural Networks}
The next step is classifying multiple classes, in this example four:

\begin{figure}[H]
    \centering
    \includegraphics[width=0.5\textwidth]{figures/two_perceptrons}
    \caption{Four classes divided by two lines}
    \label{fig:twoPerceptrons}
\end{figure}

As shown in the figure, this can be obtained using two lines or more specifically two perceptrons.
The predicted class would then be chosen from the combined predictions as follows:

\begin{table}[H]
    \centering
    \begin{tabular}{r r r}
        \textbf{Predicted Class} & \textbf{First output} & \textbf{Second output} \\
        0 & 0 & 0 \\
        1 & 0 & 1 \\
        2 & 1 & 0 \\
        3 & 1 & 1 \\
    \end{tabular}
\end{table}

\begin{figure}[H]
   \centering
   \includegraphics[width=0.15\textwidth]{figures/two_perceptrons_net}
   \caption{Two perceptrons receive the same inputs but classify differently}
   \label{fig:twoPerceptronsNet}
\end{figure}

\par This already shares similarities with a neural network and adding more perceptrons enhances this phenomenon.
The remaining differences from a complete neural network the following:
\begin{itemize}
        \item Chain multiple perceptrons after each other in layers to learn sub-decisions or abstractions.
        \item Introduce activation functions to apply on the term $wx + b$ to add non-linearities.
        \item Define an error function which serves as a measure of how good the prediction is. By using this error function, the parameters may be updated to find a local minimum (backpropagation)~\cite{backpropagation}.
\end{itemize}

\section{Reinforcement Learning}\label{sec:rl}
The activity of having a class assigned to each sample and changing the weights to match these classes is called supervised learning (SL).
\par Another type is reinforcement learning (RL).
It is applied in problems where agents perform actions in an environment and attempt to maximize the cumulative reward of a run.
\begin{figure}[H]
   \centering
   \includegraphics[width=0.65\textwidth]{figures/agent}
   \caption{An agent performs action in an environment}
   \label{fig:agent}
\end{figure}
A very simple example for this is a video game where the player aims to reach a maximum score.
In these scenarios there is no predetermined solution that needs to be approached.
Instead the decision making process is viewed as a Markov Decision Process (MDP).
An MDP is defined as a tuple containing~\cite{rl}:
\begin{itemize}
    \item A set of states $S$, i.e.\ the current state of the environment e.g.\ the board position in chess
    \item A set actions $A$ that lead the system from one state $s$ to another state $s'$, e.g.\ moving a piece
    \item The probability function $p(s' | s,a)$ that the system goes to state $s'$ after performing the action $a$ in state $s$
    \item The reward $R(s', s,a)$ that is obtained after transitioning from state $s'$ to state $s$ after performing action $a$.
\end{itemize}

The goal is to maximize the cumulative reward that is represented by this formula:
\[
    \sum_{k=0}^{\infty} \gamma^k r_{k + 1} 
\]
where $\gamma \in [0, 1]$ is a discount factor that prioritizes earlier rewards more than later ones.
This approach is chosen because it is often not possible to define an immediate reward.
For example a move in a chess game may not be obvious as the ``winning move'' without including but the reward of future moves.
\vspace{1em}
\par This formula is not analytically solvable.
So the derivation of the maximum is typically handled by learning an optimal policy that chooses the best action given a certain state.
Without going into too much detail this is mostly done by computing an expected reward with a neural network for every possible action and then choosing the best one.
The neural network learns by using the real rewards as targets or correct prediction.

\subsection{Exploration and Exploitation}
In the beginning everything about the reward landscape is unknown.
Thus it needs to be explored to add some data points that give the first insights into what are good or bad solutions.
\par As the main goal is to find good solutions, it makes sense to focus one's attention to regions where those solutions are in the vicinity.
This presumes that results with higher accuracy are similar to other results with high accuracy.
The step of applying the learned behavior is called exploitation.
\par The problem of finding the optimal solution for this exploration-exploitation tradeoff is called the multi-armed bandit problem.
One way to solve this problem is the epsilon-greedy approach with a decreasing epsilon.
At every time step the best action (i.e.\ the predicted one) is chosen with the probability $(1-\epsilon)$ and a completely random one otherwise~\cite{rbed}.
\par For the purpose of exploring more in the beginning and less in the end the epsilon starts with the value one.
After that it is discounted with a factor between zero and one after each generation until it reaches a certain threshold in the vicinity of zero.
\begin{figure}[H]
   \centering
   \includegraphics[width=0.75\textwidth]{figures/decisions}
   \caption{During training random actions decrease and predicted actions increase}
   \label{fig:decisions}
\end{figure}

\section{Upside Down Reinforcement Learning}\label{sec:udrl}
\par Upside Down Reinforcement Learning (UDRL) is a method that transforms reinforcement learning into supervised learning. 
\par In contrast to reinforcement learning, UDRL does not predict rewards at all.
Instead it interprets them as input commands and predicts actions, ``turning traditional RL on its head''~\cite{udrl}.
Additionally to the observations and last performed actions, an agent receives a desired reward and the time horizon, in which this reward has to be obtained, as inputs.
Over time the agent learns which actions it has to perform to reach the desired rewards.
\par Using a time horizon allows to create many more samples for learning than traditional RL.\@
A sample in this context is the combination of the starting step and a target step that is associated with a certain reward.
\begin{figure}[H]
   \centering
   \includegraphics[width=0.75\textwidth]{figures/number_of_episodes}
   \caption{Five steps in one completed run or episode, resulting in ten usable samples}
   \label{fig:numEpisodes}
\end{figure}
As visualized in Figure~\ref{fig:numEpisodes} the resulting number of samples increases quadratically with the number of steps $n$ in the episode as described by this formula:
\begin{equation}
n - 1 + n - 2 + \cdots + 3 + 2 + 1 = \frac{n(n-1)}{2}
\end{equation}
This means only one episode can lead to a lot of data that can be used to learn many different scenarios with different time horizons and resulting rewards.
\par Furthermore, this mode of operation allows for selecting highly dimensional actions and makes predicting continuous action easier.
Traditionally, reinforcement learning tries to predict the reward for each action.
This means that if an action is comprised of multiple outputs that can all be independently activated or not, it is necessary to calculate the rewards for every possible combination of those aforementioned outputs.
\par To be more precise in the discrete case, the following formula describes the number of actions $|A|$ which rewards need to be computed:
\begin{equation}
|A| = \prod_{a \in A}{|a|}
\end{equation}
with $|a|$ being the number of outputs for action $a$.
\par For example a scenario with four outputs, of which each one can be either switched on or off, leads to $2^4 = 16$ possible actions.
Once the count of outputs reaches a number in the hundreds this approach is not feasible anymore and cannot be used with today's technology.
\par If the actions are directly predicted as it is done in UDRL, it leads to an exponential speedup as the number of calculations is now $O(1)$.
\par Usually the goal of RL is to reach high rewards over extended periods of time, which implies using high rewards and long time horizons as inputs.
To achieve good learning metrics the reward is set to an upper bound depending on the last rewards that is higher than previously seen ones.
The time horizon is typically set to double the current live-time or length of the episode up until now~\cite{udrlTraining}.
During the exploration the output is changed randomly or with another algorithm.
\par Once having executed a few episodes this way, the resulting data set is fed to the learning algorithm with the rewards and time horizons that really occurred.
Now the loss between the now predicted actions and the actually executed ones is used to approximate both outputs.
\begin{figure}[H]
   \centering
   \includegraphics[width=0.85\textwidth]{figures/udrl}
   \caption{Visualization of the UDRL algorithm}
\end{figure}\label{fig:udrl}
\begin{algorithm}
    \caption{Upside Down Reinforcement Learning}
    \begin{algorithmic}
        \While{stopping criterion not reached}
            \If {Exploring}
                \State \textit{Action 1} $\gets \text{Random}()$
            \Else
                \State \textit{Action 1} $\gets \text{NN}(\text{target reward}, \text{time horizon}, \text{observations})$
            \EndIf
            \State Perform \textit{Action 1} in the environment
            \State The achieved reward differs from the target reward.
            \State \textit{Action 2} $\gets \text{NN}(\text{achieved reward}, \text{time horizon}, \text{observations})$
            \State Compute loss and perform backpropagation \textit{Action 2} $\rightarrow$ \textit{Action 1}
        \EndWhile
    \end{algorithmic}
\end{algorithm}

\section{Neural Network Layers}
Layers are one of the predefined blocks in a neural network.
They determine how the neurons are interconnected.
Promising ones have been already evaluated frequently in real world scenarios~\cite{maxPool}\cite{CNNSurvey}\cite{batchnorm}.
\subsection{Linear Layers}\label{sec:linear}
Linear layers are the simplest type of layers, here every neuron of the previous layer is connected to every neuron in the output layer.
Hence they are also known as fully connected layers.
They are essentially a scaled up version of Figure~\ref{fig:twoPerceptronsNet}.
\begin{figure}[H]
   \centering
   \includegraphics[width=0.15\textwidth]{figures/fc}
   \caption{All neurons are interconnected between layers}
\end{figure}\label{fig:fc}
One big disadvantage of these layers is the high number of parameters.
\subsection{Convolutional Layers}\label{sec:cnn}
Convolutional Neural Networks were introduced in the first version of LeNet in 1989~\cite{CNNSurvey}.
As the name implies they are able to extract features using convolutional operations.
Weight sharing and local connections mean that the number of parameters is greatly reduced as unlike in a fully connected layer not every neuron is connected the every neuron in the following layer.
\begin{figure}[H]
   \centering
   \includegraphics[width=0.75\textwidth]{figures/cnn_layer}
   \caption{The typical hyperparameters of a CNN layer}
\end{figure}\label{fig:cnnLayer}
\par Typically a convolutional layer has the following parameters:
\begin{itemize}
\item Kernel size: A kernel corresponds to the area that is involved in the calculation of one convolution, typically a rectangle.
\item Stride: The stride corresponds to the distance between kernels in the input data, a higher stride leads to less dense convolution.
\item Padding: At the border some kernels (with the size greater one) convolutions cannot be applied on every input. This is where padding comes in. It pads the input with a certain number of pixels to include every original pixel in the calculations.
\item Number of input layers: This is the number of dimensions of the input data, in an RGB image it would be three values. In higher layers of a convolutional network it is the number of output layers of the previous layer.
\item Number of output layers: This is the number of output dimensions of this layer and corresponds to the quantity of learned kernels.
\item Dilation: In order to facilitate the observation of a larger area dilation can be introduced. A dilation of \textit{n} fills the space between kernel points with $n - 1$ empty (zero-filled) pixels.
\end{itemize}
\begin{figure}[H]
   \centering
   \includegraphics[width=0.75\textwidth]{figures/cnn_dilation}
   \caption{a) default 1-dilation, b) 2-dilation and c) 3-dilation}
\end{figure}\label{fig:cnnDilation}
\par The output height (and width respectively) of a convolutional layer is calculated as such~\cite{cnnCalc}:
\begin{equation}\label{eq:cnnSize}
    \text{size}_{\text{out}} = \lfloor \frac{\text{size}_{\text{in}} + 2 \cdot \text{padding} - \text{dilation} \cdot (\text{kernel} - 1)  - 1}{\text{stride}} + 1\rfloor
\end{equation}
\subsection{MaxPool Layers}
Max pooling layers were first introduced for time alignment in \textit{A neural network for speaker-independent isolated word recognition} by \textit{Kouichi Yamaguchi} published in 1990~\cite{maxPool}.
\par Instead of using convolutions on a region, it selects the maximum value from multiple inputs.
It does so for each input layer, meaning that the number of output layers corresponds to the number of input layers.
\subsection{Dropout Layers}
Dropout layers are a solution to avoid overfitting.
Previously it was dealt with by combining the prediction of multiple large neural networks~\cite{dropout}.
Now the same is achieved by randomly dropping weights and their associated connections with the probability $p$ and thus providing the same effect as multiple networks.
\par After a network has been trained and it needs to be applied on e.g.\ the test set, the dropout layer is removed and the weights of the previous layer are adapted to make up for the additional connections, i.e.:
\begin{equation}
w_{\text{new}} = w_{\text{old}} \cdot (1 - p)
\end{equation}
\subsection{Softmax Layers}\label{sec:softmax}
A Softmax layer is a continuous and differentiable version of the arg max function.
This function provides a one-hot vector that has a one at the same index as the maximum value in the input-vector and a zero in all other places.
\begin{figure}[H]
    \begin{equation}
        \text{argmax}([-0.2, 0.4, 0.2, 0.5]) = [0.0, 0.0, 0.0, 1.0]
    \end{equation}
    \caption{An example for the arg max function}
\end{figure}
It is primarily used in predicting the class in multiclass classification problems.
\par The formula is defined as:
\begin{equation}
    \text{Softmax}{(z)}_i = \frac{\exp(z_i)}{\sum_j{\exp(z_j)}}
\end{equation}
\par There is also the LogSoftmax, that applies the log after the Softmax, which penalizes wrong predictions more heavily and leads to a numerically more stable solution:
\begin{equation}
    \text{LogSoftmax}{(z)}_i = z_i - \log{\sum_j{\exp(z_j)}}
\end{equation}
\subsection{BatchNorm Layers}
A batch normalizing transform aims to make the optimization landscape significantly smoother~\cite{batchnormWork}.
\par It works by computing the mean and the variance of the inputs of the current mini-batch with the size $m$,
\begin{equation}
    \mu_B = \frac{1}{m} \sum_{i=1}^m x_i
\end{equation}
\begin{equation}
    \sigma_B^2 = \frac{1}{m} \sum_{i=1}^m {(x_i - \mu_B)}^2
\end{equation}
 using the results to obtain a normalized intermediate result,
\begin{equation}
    x_i' = \frac{x_i - \mu_B}{\sqrt{\sigma_B^2 + \epsilon}}
\end{equation}
and using learned parameters $\gamma$ and $\beta$ to reintroduce representational features that were potentially lost.
\begin{equation}
    y_i = \gamma \cdot x_i' + \beta
\end{equation}
\par This process is repeated for every input dimension individually~\cite{batchnorm}.

\subsection{Activation Functions}
Activation functions are mathematical operations that are performed element-wise on each output of the previous layer.
\par Over time a few of them proved to be efficient.
Of those only a few were included in this work:
\begin{itemize}
    \item Rectified Linear Unit (ReLU)
    \item Sigmoid function
    \item Hyperbolic Tangent (tanh)
\end{itemize}
\paragraph{ReLU}
The rectified linear unit (ReLU) is a function that keeps positive values and sets negative values to zero.
\begin{equation}
\text{ReLU}(x) = \max(0, x)
\end{equation}
\paragraph{Sigmoid}
The sigmoid function has a ``S''-shaped curve and outputs values between zero and one.
\begin{equation}
S(x) = \frac{1}{1 + \exp(-x)}
\end{equation}
\paragraph{Tanh}
The hyperbolic tangent has a ``S''-shaped curve and outputs values between minus one and one.
\begin{equation}
\tanh(x) = \frac{\exp(x) - \exp(-x)}{\exp(x) + \exp(-x)}
\end{equation}
Its gradient at zero is higher than the gradient of the sigmoid function.

\subsection{Neural Architecture Search}
Neural architecture search is the process of assembling a neural network out of different layers and activation functions to find solutions that learn well on particular problems.

\section{Genetic Algorithms}
Genetic algorithms are metaheuristics for finding near optimal solutions in multimodal landscapes~\cite{genetic}.
They are inspired by the natural process of evolution as they are able to direct their search into promising directions. 
\par Typically a genetic algorithm features the following steps:
\begin{itemize}
    \item Selection: Select the strongest individuals with a higher probability
    \item Mutation: Change individual ``genes''
    \item Crossover: Mix genes of multiple individuals
\end{itemize}
The intuition is to always take the best solution and change it slightly for potential improvement.
If this is the case the new solution is taken as the new starting point.
When repeating this step until convergence, it means that a local (or global, depending on the landscape) optimum is reached.
\par Implementation details are discussed more thoroughly in Section~\ref{sec:geneticAlgorithm}.

\chapter{Related Work}\label{chap:relatedWork}
\section{Neural Architecture Search With Reinforcement Learning}
The paper that has the most resemblance with this thesis is \textit{Neural Architecture Search with Reinforcement Learning} by \textit{Barret Zoph} and \textit{Quoc V. Le}~\cite{NAS_RL}.
\par It uses a recurrent neural network to predict a series of tokens, that are then translated into layers and hyperparameters. There is also a maximum number of layers after which the prediction is stopped. This horizon is extended over the course of the training.
The predictor network employs a policy gradient method called REINFORCE to learn to predict better and better child networks~\cite{reinforce}.
Each token is an action and the reward signal is the accuracy of the resulting network applied on a held-out dataset.
\par Because this training takes a lot of time, many child-networks are trained in parallel on multiple machines. The results are then sent to a controller that creates minibatches, calculates the gradients and synchronizes them with other controllers.
\par One key feature that is represented in this paper but not in this thesis is the possibility to create residual neural networks or networks with multiple inputs, branching and merging operations.
\par In the end after applying this process on CIFAR-10 with preprocessing and data augmentation, they achieved a minimum error rate of 3.65\%.
\vspace{1em}
\par A similar approach is \textit{Designing Neural Network Architectures Using Reinforcement Learning} by \textit{Bowen Baker}~\cite{NASdesignRL}.
However, this publication only focuses on Convolutional Neural Networks in conjunction with Q-Learning and $\epsilon$-greedy exploration.
It restricts itself on just predicting convolutional, pooling and fully connected layers. It does not calculate activation functions or any other layer types, therefore not allowing features like skip connections in residual neural networks.
\par After training on the CIFAR-10 dataset they achieved an error rate of 6.92\%.
\par When comparing this paper to this thesis it uses a smaller action space as the number of different layers is smaller.
Furthermore instead of UDRL it employs Q-Learning which does not have the potential of learning complex correlations.
This correlations allow to learn extrapolation and how different choices regarding the layers influence the achieved reward.

\section{Evolving Neural Networks through Augmenting Topologies}
The method called NeuroEvolution of Augmenting Topologies (NEAT) deals with the question of how to evolve network topologies along with weight updates~\cite{neat}
Instead of using backpropagation the weights are adjusted using a genetic algorithm.
\par The structure of the neural network is abstracted using direct encoding.
Here neurons or nodes and connections and their associated weights are specified explicitly with source and end node.
The network starts with the simplest possible configuration that is a fully connected one with the predetermined number of input and output nodes.
\par Another innovation of this paper is the introduction of speciation.
This concept keeps track of the how mutations propagate and groups them into species.
Within these species recombination in the form of cross over can take place which counteracts muller's ratchet~\cite{mullers_ratchet}.
These recombination could otherwise not take place as the genomes would be incompatible.
Another advantage is the ``protection of innovation''.
Even if a species does not have the best performance it is still kept, giving it the chance to improve in the future.
\par The final algorithm looks like this:
\begin{algorithm}
    \caption{NEAT}
    \begin{algorithmic}
        \State Initialize all nets minimally (without hidden units)
        \While{stopping criterion not reached}
            \For{each network}
                \State Mutate network
                \State Evaluate network
            \EndFor
            \State Group networks into species
            \State Delete the worst performing 50\% of each species
            \State Recombine the remaining networks to create children
        \EndWhile
    \end{algorithmic}
\end{algorithm}
\par The mutation of a network can occur in different ways~\cite{neat-python}:
\begin{table}[H]
    \centering
    \begin{tabularx}{\textwidth}{l | X}
        \textbf{Instruction} & \textbf{Description} \\
        \hline
        add\_connection & add new connection with a random weight between two previously unconnected nodes \\ \hline
        add\_node & add a new node in the ``middle'' of a connection, the first weight becomes 1 and the second keeps the old weight \\ \hline
        delete\_connection & disable a connection between two nodes \\ \hline
        delete\_node & delete a node and all associated connections \\ \hline
        weight shift & change weight by adding or subtracting a number \\ \hline
        weight randomization & weight is set to a random value \\ \hline
    \end{tabularx}
\end{table}
\par In contrast to this thesis this paper focuses mainly on learning reinforcement learning tasks.
The changes that are possible in the networks are nevertheless more flexible.
Connections can be arbitrarily added and deleted and are not limited to predefined layer types.
\par On the other hand the resulting networks are highly specialized for the problem they were optimized for.
NEAT also performs better when creating small neural networks compared to large and deep ones.
Creating regular features that exhibit properties such as translational invariance, e.g.\ when combining convolutional layers with MaxPool layers, are very difficult to evolve.
The same can be said for a higher number of layers.
Here the number of possible mutations skyrockets and the reward landscape suddenly becomes very sparse.

\section{AutoML-Zero}
A recent very ambitious work by Google is AutoML Zero~\cite{autoMLZero}.
Until now all network architecture search focused on the using layers designed by experts as building blocks to restrict the search space.
It uses an evolutionary approach to start literally from zero (hence the name) with as little human influence as possible. Instead of already having predefined features such as layers, activation functions or even backpropagation, it only employs basic vectorized mathematical operations in three distinct functions that are empty in the beginning:
\begin{algorithm}
    \caption{AutoML Zero}
    \begin{algorithmic}
        \State Initialize the functions Setup, Predict and Learn with random instructions
        \While{stopping criterion not reached}
        \For{each algorithm}
            \State Setup: Define and initialize the variables
            \For{each sample in the training set}
                \State Predict: Execute instructions to predict
                \State Learn: Execute instructions to learn
            \EndFor
            \For{each sample in the validation set}
                \State Predict: Execute instructions to predict
            \EndFor
            \State Compute loss on the validation set
        \State Select the best algorithms using \textit{regularized evolution}
        \State Change the instructions in all functions randomly
        \EndFor
        \EndWhile
    \end{algorithmic}
\end{algorithm}
The biggest challenge when employing this procedure is the sparsity of  the search space:
With random search finding an algorithm that performs better than the previous one, the probability lies between $10^{-7}$ and $10^{-12}$.
Evolution improves these probabilities to $5.6 \cdot 10^{-7}$ and $2.3 \cdot 10^{-8}$ respectively (which still is rather low).
\par Using this approach on a modified minimized CIFAR-10 dataset the algorithm ``discovered'' backpropagation, linear models, the rectified linear unit (ReLU) and stochastic gradient descent (SGD) and reached an accuracy of 76\%.
\par Compared to this thesis Google's approach is much more generalized.
The only aspect that is predetermined is the pool of vector operations that can be used build the algorithm.
However, so many degrees of freedom lead to relatively low final accuracy, even with the amount of resources at Google's disposal.
