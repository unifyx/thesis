#!/bin/python

import sys
import os
from glob import glob
sys.path.insert(1, os.getcwd() + '/../Sources/')
import plotter

outpath = 'figures/' + ''.join(__file__.split('/')[-1].split('.')[:-1]) + '.pdf'

files = glob('./figures/data/cifar_gene?.res')
plotter.draw_files(files, outpath, x_label='Generations', y_label='Accuracy', scale=True, smoothing=100)
