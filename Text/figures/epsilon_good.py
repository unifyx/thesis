#!/bin/python

import sys
import os
#sys.path.insert(1, '../../Sources/')
sys.path.insert(1, os.getcwd() + '/../Sources/')
import plotter

outpath = 'figures/' + ''.join(__file__.split('/')[-1].split('.')[:-1]) + '.pdf'

plotter.draw(functions=[lambda x : 0.998 ** x], function_names=['epsilon (fast decay)'], \
        interval=(0, 10000), filename=outpath, x_label='Generation', y_label='Probability')
