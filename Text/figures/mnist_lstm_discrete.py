#!/bin/python

import sys
import os
from glob import glob
sys.path.insert(1, os.getcwd() + '/../Sources/')
import plotter

outpath = 'figures/' + ''.join(__file__.split('/')[-1].split('.')[:-1]) + '.pdf'

filegroups = [
        ('genetic algorithm', glob('./figures/data/mnist_gene?.res')),
        ('average reward * 1.2', glob('./figures/data/mnist_lstm_average_reward?.res')),
        ('maximum reward', glob('./figures/data/mnist_lstm_max_reward?.res')),
        ]

plotter.draw_file_groups(filegroups, outpath, x_label='Generations', y_label='Accuracy', scale=True, smoothing=100, smooth_cut=False)
