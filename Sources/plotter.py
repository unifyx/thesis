#!/bin/python

import seaborn as sns
import pandas

num_points = 1000

def draw(x_data=None, y_datas=None, smoothing=1, smooth_cut=False, \
        x_label='x', y_label='y', \
        functions=None, function_names=None, interval=(0,1), \
        filename='/ramdisk/test.svg'):
    if y_datas is None and functions is None:
        raise ValueError("y_data or functions need to be defined")
    if y_datas is not None and smoothing > 1:
        for i, y_data in enumerate(y_datas):
            s = [sum(y_data[max(0, x - smoothing):min(x+smoothing, len(y_data) - 1)]) /
                    (min(x+smoothing, len(y_data) - 1) - max(0, x - smoothing)) \
                    for x in range(len(y_data))]
            if smooth_cut:
                y_data = s[smoothing:-smoothing]
            else:
                y_data = s
            y_datas[i] = y_data

    if x_data is None:
        if y_datas is not None:
            maxlen = min(len(y) for y in y_datas)
            x_data = list(range(1, maxlen + 1))
        else:
            x_data = [interval[0] + x / num_points * (interval[1] - interval[0]) \
                    for x in range(num_points)]

    values = []
    if y_datas is not None:
        for y_data in y_datas:
            values += [['data', x, y_data[x-1]] for x in x_data]
    if functions is not None:
        for n, f in zip(function_names, functions):
            def g(x):
                try:
                    return f(x)
                except:
                    return None

            values += [[n, x, g(x)] for x in x_data]
    df = pandas.DataFrame(values, columns=['function', 'x', 'y'], dtype=float)

    plot = sns.lineplot(data=df, x='x', y='y', hue='function')
    plot.legend().set_title('')
    plot.set(xlabel=x_label, ylabel=y_label)
    plot.figure.savefig(filename)
    plot.figure.clf()

def scale_list(l, n):
    old_len = len(l)
    r = []
    for i in range(n):
        if n > old_len:
            r.append(l[int(i*old_len/n)])
        else:
            r.append(sum(l[int(i*old_len/n):int((i+1)*old_len/n)])/(old_len/n))
    return r

def draw_files(files, output, x_label='x', y_label='y', scale=False, smoothing=50):
    data = []
    for filename in files:
        res = open(filename, 'r')
        data.append([float(x) for x in res.readlines()])
        res.close()
    if scale:
        new_len = int(sum(len(l) for l in data) / len(data))
        for i, l in enumerate(data):
            data[i] = scale_list(l, new_len)
    draw(y_datas=data, smooth_cut=True, smoothing=smoothing, filename=output, \
            x_label=x_label, y_label=y_label)

def draw_file_groups(filegroups, output, x_label='x', y_label='y', scale=False, smoothing=50, smooth_cut=True):
    final_dataframe = pandas.DataFrame(columns=['x', 'y', 'function'])
    for data_name, files in filegroups:
        data = []
        for filename in files:
            res = open(filename, 'r')
            data.append([float(x) for x in res.readlines()])
            res.close()
        if scale:
            new_len = int(sum(len(l) for l in data) / len(data))
            for i, l in enumerate(data):
                data[i] = scale_list(l, new_len)
        complete_list = []
        for i, y_data in enumerate(data):
            s = [sum(y_data[max(0, x - smoothing):min(x+smoothing, len(y_data) - 1)])/(min(x+smoothing, len(y_data) - 1)-max(0, x - smoothing)) \
                    for x in range(len(y_data))]
            if smooth_cut:
                y_data = s[smoothing:-smoothing]
            else:
                y_data = s
            complete_list += list(enumerate(y_data))

        df = pandas.DataFrame(complete_list, columns=['x', 'y'])
        df['function'] = data_name
        final_dataframe = pandas.concat((final_dataframe, df))

    plot = sns.lineplot(data=final_dataframe, x='x', y='y', hue='function')
    plot.legend().set_title('')
    plot.set(xlabel=x_label, ylabel=y_label)
    plot.figure.savefig(output)
    plot.figure.clf()
