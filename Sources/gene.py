#!/bin/python

import random

from utils import get_cifar, create_net, train_model, delete_logs, mutate_parameters

experiment_name = ''.join(__file__.split('/')[-1].split('.')[:-1])
delete_logs(experiment_name)


max_layers    = 3
num_types     = 6
num_params    = 6
batch_size    = 64

decay_bad     = 0.9995
decay_good    = 0.998
threshold_inc = 0.001

population    = 10
num_elites    = 2

trainset, testset, image_shape, num_labels = get_cifar(batch_size)
#trainset, testset, image_shape, num_labels = get_mnist(batch_size)


class GeneticAlgorithm():
    def __init__(self, population_size):
        self.epsilon       = 1.0
        self.threshold     = 0.1

        self.population_size = population_size
        self.population      = [Candidate() for _ in range(population_size)]
        self.fitness         = [1.0 for _ in range(population_size)]

        self.mutation_rate   = self.epsilon
        self.type_mutation   = self.epsilon
        self.crossover_prob  = self.epsilon

    def evaluate(self):
        self.fitness = []
        for candidate in self.population:
            model, _ = create_net(candidate.parameters, image_shape, \
                    num_types, experiment_name, num_labels)
            self.fitness.append(train_model(model, trainset, testset, \
                    experiment_name, num_episodes=-1))

        good_results = len([x for x in self.fitness if x > self.threshold])
        bad_results  = self.population_size - good_results
        self.epsilon     *= (decay_good ** good_results) * (decay_bad ** bad_results)
        self.threshold   += threshold_inc * good_results

        #to be changed if values need to be different
        self.mutation_rate   = self.epsilon
        self.type_mutation   = self.epsilon
        self.crossover_prob  = self.epsilon

    def select(self):
        self.population = sorted(zip(self.population, self.fitness), key=lambda x: -x[1])[:2]
        self.population = [x[0] for x in self.population]
        #Attention: the following only works properly if elites == 2!
        for _ in range(int((self.population_size - num_elites) / 2)):
            self.population.append(Candidate(self.population[0].parameters))
            self.population.append(Candidate(self.population[1].parameters))
            if random.random() < self.crossover_prob:
                if random.random() < 0.5:
                    self.population[-1].crossover(self.population[-2])
                else:
                    self.population[-2].crossover(self.population[-1])
        for candidate in self.population[2:]:
            mutate_parameters(candidate.parameters, self.epsilon, max_layers, num_types, num_params)

class Candidate():
    def __init__(self, parameters=None):
        if parameters is None:
            self.parameters = []
            for _ in range(max_layers):
                one_hot = [0. for _ in range(num_types)]
                one_hot[random.randrange(num_types)] = 1.
                self.parameters.append(one_hot + [random.random() for _ in range(num_params)])
        else:
            self.parameters = [x[:] for x in parameters]

    def crossover(self, other_candidate):
        crossover_point = random.randrange(max_layers)
        self.parameters = [x[:] for x in self.parameters[:crossover_point]] + \
                [x[:] for x in other_candidate.parameters[crossover_point:]]

genAl = GeneticAlgorithm(population)

genAl.evaluate()
while genAl.epsilon > 0.02:
    genAl.select()
    genAl.evaluate()
