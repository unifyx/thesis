#!/bin/python

import torch
import torch.nn as nn
import torch.optim as optim

from utils import create_net, get_cifar, train_model, delete_logs, mutate_parameters

experiment_name = ''.join(__file__.split('/')[-1].split('.')[:-1])
delete_logs(experiment_name)

max_layers    = 3
meta_batch    = 5
decay_bad     = 0.9995
decay_good    = 0.998
threshold     = 0.1
threshold_inc = 0.001

num_types     = 6
num_params    = 6
layer_size    = num_types + num_params

trainset, testset, image_shape, num_labels = get_cifar(64)
#trainset, testset, image_shape, num_labels = get_mnist(64)

class MetaNetwork(nn.Module):
    def __init__(self):
        super().__init__()

        self.lin1    = nn.Linear(1, 16)
        self.lin2    = nn.Linear(16, 64)
        self.lin3    = nn.Linear(64, max_layers * (num_types + num_params))

        self.sigmoid = nn.Sigmoid()
        self.relu    = nn.ReLU()
    def forward(self, x):
        x = self.relu(self.lin1(x))
        x = self.relu(self.lin2(x))
        x = self.sigmoid(self.lin3(x))

        return x

meta_learner = MetaNetwork()

def get_parameters(reward, uncertainty):
    reward = torch.Tensor([[reward]])
    parameters = meta_learner(reward)

    if uncertainty > 0.:
        parameters = parameters.detach()
        p_list2 = [parameters[0][i*(num_types + num_params):(i+1)*(num_types + num_params)] \
                for i in range(max_layers)]
        mutate_parameters(p_list2, uncertainty, max_layers, num_types, num_params)
        parameters = torch.cat(p_list2, 0).view(1, -1)
    return parameters

epsilon = 1.0
meta_criterion = nn.MSELoss()
meta_optimizer = optim.Adam(meta_learner.parameters(), lr = 0.01)
while epsilon > 0.02:
    learners = []

    for _ in range(meta_batch):
        p = get_parameters(min(1., 1.0), epsilon)
        p_list = [p[0][i*(num_types + num_params):(i+1)*(num_types + num_params)] \
                for i in range(max_layers)]
        net, dparams = create_net(p_list, image_shape, num_types, experiment_name, num_labels)
        dparam_list = torch.cat(dparams, 0).view(1, -1)
        learners.append((net, dparam_list))

    results = []
    for model in [x[0] for x in learners]:
        results.append(train_model(model, trainset, testset, experiment_name))

    #learn with results
    outputs = []
    targets = []

    for index, result in enumerate(results):
        p = get_parameters(result, 0.)
        outputs += p
        targets += learners[index][1]

    outputs = torch.stack(outputs)
    targets = torch.stack(targets)

    meta_optimizer.zero_grad()
    loss = meta_criterion(outputs, targets)
    loss.backward()
    meta_optimizer.step()

    good_results = len([x for x in results if x > threshold])
    bad_results  = meta_batch - good_results
    epsilon     *= (decay_good ** good_results) * (decay_bad ** bad_results)
    threshold   += threshold_inc * good_results
