#!/bin/python

import os
import sqlite3
import random
import torch
import torch.nn as nn
import torch.optim as optim
from torchvision import datasets, transforms

from progress import Progress
from generators import GeneratorConv, GeneratorLinear, GeneratorMaxPool, \
    GeneratorDropout, GeneratorBatchNorm, GeneratorEmpty, \
    LayerLast, LayerConv, LayerEmpty, LayerLinear, LayerMaxPool, LayerDropout, LayerBatchNorm

# ============================================ #
#                   CLASSES                    #
# ============================================ #

db_name = 'networks.db'

class Learner(nn.Module):
    def __init__(self):
        super().__init__()
        self.layers = nn.ModuleList()
    def forward(self, x):
        for layer in self.layers:
            x = layer(x)
        return x

# ============================================ #
#                  FUNCTIONS                   #
# ============================================ #

def delete_logs(experiment_name):
    for ext in ['err', 'net', 'res']:
        if os.path.exists(f"{experiment_name}.{ext}"):
            os.remove(f"{experiment_name}.{ext}")

def get_mnist(batch_size):
    transform = transforms.Compose([transforms.ToTensor(),
                                  transforms.Normalize((0.5,), (0.5,)),
                                  ])

    trainset = datasets.MNIST('data/mnist_train', download=True, train=True, \
            transform=transform)
    valset = datasets.MNIST('data/mnist_test', download=True, train=False, \
            transform=transform)
    trainloader = torch.utils.data.DataLoader(trainset, batch_size=batch_size, shuffle=True)
    valloader = torch.utils.data.DataLoader(valset, batch_size=batch_size, shuffle=True)

    for image, _ in trainloader:
        image_shape = image.shape
        break

    return trainloader, valloader, image_shape, 10

def get_cifar(batch_size):
    transform = transforms.Compose([transforms.ToTensor(),
                                  transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5)),
                                  ])

    trainset = datasets.CIFAR10('data/cifar10_train', download=True, train=True, \
            transform=transform)
    valset = datasets.CIFAR10('data/cifar10_test', download=True, train=False, \
            transform=transform)
    trainloader = torch.utils.data.DataLoader(trainset, batch_size=batch_size, shuffle=True)
    valloader = torch.utils.data.DataLoader(valset, batch_size=batch_size, shuffle=True)

    for image, _ in trainloader:
        image_shape = image.shape
        break

    return trainloader, valloader, image_shape, 10

def train_model(model, trainset, testset, experiment_name, num_episodes=-1):
    if model is None:
        return 0.

    if not os.path.exists(db_name):
        create_db()
    db_connection = sqlite3.connect(db_name)
    db_cursor = db_connection.cursor()
    description = describe_net(model)

    max_samples = 3

    rows = db_cursor.execute(
        "SELECT Samples, Accuracy FROM Results WHERE Description = ?;",
        (description,),
        ).fetchall()

    new_net = True
    if len(rows) != 0:
        new_net = False
        samples = rows[0][0]
        accuracy = rows[0][1]
        if samples == max_samples:
            db_cursor.close()
            db_connection.close()
            return accuracy


    criterion = nn.CrossEntropyLoss(reduction='mean')
    optimizer = optim.Adam(model.parameters(), lr = 0.01)

    episode = 0
    last_result = 0.

    while True:
        p = Progress('Train')
        counter = 0
        maxc = len(trainset)
        model.train()
        try:
            for images, labels in trainset:
                counter += 1
                p.update(counter / maxc)
                optimizer.zero_grad()
                loss = 0
                output = model(images)
                target = labels
                loss = criterion(output, target)
                loss.backward()
                optimizer.step()
        except RuntimeError as e:
            with open(f"{experiment_name}.err", 'a') as f:
                f.write(str(e) + '\n')
            if new_net:
                db_cursor.execute(
                    '''INSERT OR IGNORE INTO Results (Description, Samples, Accuracy)
                    VALUES (?, ?, ?);''',
                    (description, 1, 0.))
            else:
                db_cursor.execute(
                    'UPDATE Results SET Samples = ?, Accuracy = ? WHERE Description = ?;',
                    (samples + 1, accuracy/(samples + 1), description))
            db_connection.commit()
            db_cursor.close()
            db_connection.close()
            return 0.

        p.end()

        counter = 0
        maxc = len(testset)
        correct = 0
        wrong = 0
        model.eval()
        p = Progress('Test')
        try:
            for images, labels in testset:
                counter += 1
                p.update(counter / maxc)

                out = model(images).detach().numpy()
                for i, prediction in enumerate(out):
                    if list(prediction).index(max(prediction)) == labels[i]:
                        correct += 1
                    else:
                        wrong += 1
        except:
            result = 0.
            last_result = 0.
            break

        p.end()

        result = correct/(correct + wrong)
        if num_episodes == -1:
            if 0. <= result - last_result < 0.05:
                break
            if result - last_result < 0.:
                result = last_result
                break
            last_result = result
        else:
            episode += 1
            if episode >= num_episodes:
                break


    with open(f"{experiment_name}.res", 'a') as f:
        f.write(str(result) + '\n')
    with open(f"{experiment_name}.net", 'w') as f:
        f.write(f"result: {result}\nmodel:\n{model}\n\n")

    if new_net:
        db_cursor.execute(
            '''INSERT OR IGNORE INTO Results (Description, Samples, Accuracy)
            VALUES (?, ?, ?);''',
            (description, 1, result))
    else:
        db_cursor.execute(
            'UPDATE Results SET Samples = ?, Accuracy = ? WHERE Description = ?;',
            (samples + 1, ((samples * accuracy) + result)/(samples + 1), description))

    db_connection.commit()
    db_cursor.close()
    db_connection.close()
    return result

def create_net(parameterList, input_shape, num_types, experiment_name, num_labels):
    try:
        learner = Learner()

        discrete_params = []
        for parameters in parameterList:
            parameters = torch.Tensor(parameters)
            parameters = parameters.view(-1)
            sm = parameters[:num_types]
            sm = torch.Tensor(sm).view(1, -1)
            _, smindex = torch.max(sm, 1)
            params = torch.Tensor(parameters[num_types:])

            if   smindex == 0:
                generator = GeneratorConv(input_shape, params)
            elif smindex == 1:
                generator = GeneratorLinear(input_shape, params)
            elif smindex == 2:
                generator = GeneratorMaxPool(input_shape, params)
            elif smindex == 3:
                generator = GeneratorDropout(params)
            elif smindex == 4:
                generator = GeneratorBatchNorm(input_shape)
            elif smindex == 5:
                generator = GeneratorEmpty()

            layer = generator.getLayer()
            input_shape = layer(torch.randn(input_shape)).shape
            learner.layers.append(layer)

            one_hot          = torch.zeros(num_types)
            one_hot[smindex] = 1.
            discrete_params.append(torch.cat((one_hot, generator.discrete_params), 0).view(1,-1))

        learner.layers.append(LayerLast(input_shape, numOut=num_labels))

        return learner, discrete_params
    except Exception as e:
        with open(f"{experiment_name}.err", 'a') as f:
            f.write(str(e) + '\n')
        return None, parameterList

def mutate_parameters(parameterList, mutation_rate, max_layers, num_types, num_params):
    for layer_index in range(max_layers):
        if len(parameterList[layer_index]) == 1:
            for param_index in range(num_types + num_params):
                parameterList[layer_index][0][param_index] += \
                        (random.random() * 2 - 1) * mutation_rate
                parameterList[layer_index][0][param_index] = \
                        min(max(parameterList[layer_index][0][param_index], 0), 1.)
        else:
            for param_index in range(num_types + num_params):
                parameterList[layer_index][param_index] += \
                        (random.random() * 2 - 1) * mutation_rate
                parameterList[layer_index][param_index] = \
                        min(max(parameterList[layer_index][param_index], 0), 1.)

def describe_net(model):
    descriptions = []
    for layer in model.layers:
        if isinstance(layer, LayerLast):
            pass
        elif isinstance(layer, LayerConv):
            descriptions.append(
            'C' + '_'.join(map(str, [
            layer.conv1.out_channels,
            layer.conv1.kernel_size[0],
            layer.conv1.stride[0],
            layer.conv1.padding[0],
            layer.conv1.dilation[0],
            1 if layer.conv1.bias is None else 0])))
        elif isinstance(layer, LayerEmpty):
            pass
        elif isinstance(layer, LayerLinear):
            descriptions.append(
            'L' + '_'.join(map(str, [
            layer.fc1.out_features,
            1 if layer.fc1.bias is None else 0])))
        elif isinstance(layer, LayerMaxPool):
            descriptions.append(
            'M' + '_'.join(map(str, [
            layer.maxp1.kernel_size,
            layer.maxp1.stride,
            layer.maxp1.padding,
            layer.maxp1.dilation]
            )))
        elif isinstance(layer, LayerDropout):
            descriptions.append('D' + str(int(layer.dropout1.p * 10)))
        elif isinstance(layer, LayerBatchNorm):
            descriptions.append('B')
    return '+'.join(descriptions)

def create_db():
    db_connection = sqlite3.connect(db_name)
    db_cursor = db_connection.cursor()
    db_cursor.execute('''
    CREATE TABLE IF NOT EXISTS Results (
    ID INTEGER PRIMARY KEY AUTOINCREMENT,
    Description TEXT NOT NULL,
    Samples INTEGER NOT NULL,
    Accuracy NUMERIC NOT NULL);
    ''')

    db_cursor.execute(\
            'CREATE UNIQUE INDEX IF NOT EXISTS Description_Index ON Results(Description);')
    db_connection.commit()
    db_cursor.close()
    db_connection.close()
